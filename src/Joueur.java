import java.util.*;

/**
 * Classe permettant de modeliser un joueur
 * @author Charles LADZRO (EI2I4, Polytech, 2020)
 * @version 1.0
 */
public class Joueur {

	/**
	 * le nombre de pieces enemies capturees
	 */
	private int score;

	/**
	 * le nom du joueur
	 */
	private final String nomDuJoueur;

	/**
	 * la couleur attribue au joueur
	 */
	private final String couleur;
	
	/**
	 * Definition des directions possibles
	 */
	private final String[] directions = {"bas", "gauche", "droite", "haut"};

	/**
	 * Array contenant les indices des cases que le joueur controle
	 */
	private final ArrayList<Integer> indicesDesCaseControlees = new ArrayList<>();

	/**
	 * Array contenant les indices des cases dans lesquelles le joueur peut aller
	 */
	private final ArrayList<Integer> indicesDesCaseArrivees = new ArrayList<>();

	/**
	 * Array contenant les pieces que le joueur a en reserve
	 */
	private final ArrayList<Piece> listeDesPiecesEnReserves = new ArrayList<>();

	/**
	 * Constructeur de la clase Joueur
	 * @param nomDuJoueur le nom du joueur
	 * @param couleur la couleur attribue au joueur
	 */
	Joueur(String nomDuJoueur, String couleur) { 
		this.nomDuJoueur = nomDuJoueur;
		this.couleur = couleur;
	}

	/**
	 * Retourne le score
	 * @return le score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Retourne le nom du joueur
	 * @return le nom du joueur
	 */
	public String getNomDuJoueur() {
		return nomDuJoueur;
	}

	/**
	 * Retourne la couleur du joueur
	 * @return la couleur du joueur
	 */
	public String getCouleur() {
		return couleur;
	}

	/**
	 * Change le score
	 * @param score le score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Retourne Array contenant les indices des cases que le joueur controle
	 * @return Array contenant les indices des cases que le joueur controle
	 */
	public ArrayList<Integer> getIndicesDesCaseControlees() {
		return indicesDesCaseControlees;
	}

	/**
	 * Retourne Array contenant les indices des cases dans lesquelles le joueur peut aller
	 * @return Array contenant les indices des cases dans lesquelles le joueur peut aller
	 */
	public ArrayList<Integer> getIndicesDesCaseArrivees() {
		return indicesDesCaseArrivees;
	}

	/**
	 * Retourne Array contenant les pieces que le joueur a en reserve
	 * @return Array contenant les pieces que le joueur a en reserve
	 */
	public ArrayList<Piece> getListeDesPiecesEnReserves() {
		return listeDesPiecesEnReserves;
	}

	/**
	 * @return des informations du joueur
	 */
	public String toString() {
		return "Nom joueur : " + nomDuJoueur + ", Score : " + score + ", Reserve : " 
				+ listeDesPiecesEnReserves.size() + " pieces";
	}


	/**
	 * Choix d'une case par le joueur
	 * @return l'indice de la case choisie par le joueur
	 */
	public int choisirCase() {
		
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.print("\nVeillez choisir une case que vous controlez : ");
				
				// recuperer le choix
				Scanner sc = new Scanner(System.in); 
				choix = sc.nextInt();
				if (indicesDesCaseControlees.contains(choix)) {
					ok = true;
				}
				else {
					System.out.println("\nDesole vous ne controlez pas cette case, veillez reprendre svp! ");
				}
			} 
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}

	/**
	 * Calcule les cases d'arrivees
	 * @param choixCase l'indice de la case choisie par le joueur
	 * @param choisirNombreDePiecesADeplacer le nombre de pieces a deplacer, choisie par le joueur
	 * @return le nombre de cases dans lesquelles le joueur peut aller
	 */
	public int calculerCasesArrivees(int choixCase, int choisirNombreDePiecesADeplacer) {
		
		int cpt = 0;
		
		// remove all from the list
		this.indicesDesCaseArrivees.clear();
		this.indicesDesCaseArrivees.add(-1);
		this.indicesDesCaseArrivees.add(-1);
		this.indicesDesCaseArrivees.add(-1);
		this.indicesDesCaseArrivees.add(-1);
		
		// transformer choixCase en i et j
		int i = (choixCase / 10);
		int j = choixCase - (i * 10);
		
		
		// case de gauche
		cpt += verifierEtEnregistrerCaseArrivee(j - choisirNombreDePiecesADeplacer, i, 1);
		
		// case de droite
		cpt += verifierEtEnregistrerCaseArrivee(j + choisirNombreDePiecesADeplacer, i, 2);
		
		// case de haut
		cpt += verifierEtEnregistrerCaseArrivee(j, i - choisirNombreDePiecesADeplacer, 3);
		
		// case de bas
		cpt += verifierEtEnregistrerCaseArrivee(j, i + choisirNombreDePiecesADeplacer, 0);
		
		return cpt;
	}

	/**
	 * Verifier les cases d'arrivee
	 * @param i le coordonnee x de la case choisie par le joueur
	 * @param j le coordonnee y de la case choisie par le joueur
	 * @param indice l'indice correspondant a la position haut, bas, gauche, droite
	 * @return l'etat du deplacement(0 pour impossible - 1 pour possible)
	 */
	private int verifierEtEnregistrerCaseArrivee(int i, int j, int indice) {
		int n = 0;
		if (1 <= i && i <= 6 && 1 <= j && j <= 6) {
			int val = (10 * j ) + i;
			this.indicesDesCaseArrivees.set(indice, val);
			n = 1;
		}
		else {
			this.indicesDesCaseArrivees.set(indice, -1);
		}
		
		return n;
	}

	/**
	 * Choix de la case d'arrivee par le joueur
	 * @return l'indice de la case choisie par le joueur
	 */
	public int choixCaseArrivee() {
		
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.println("\nVeillez choisir le numero de la direction a suivre : ");
				for (int i = 0; i < 4; i++) {
					if (indicesDesCaseArrivees.get(i) != -1) {
						System.out.println(2 * (i + 1) + " - " + directions[i]);
					}
				}
				System.out.print("Reponse : ");
				
				// recuperer le choix
				Scanner sc = new Scanner(System.in); 
				choix = sc.nextInt();
				if (choix == 2 || choix == 4 || choix == 6 || choix == 8){
					
					if (indicesDesCaseArrivees.get((choix / 2) - 1) != -1) {
						choix = indicesDesCaseArrivees.get((choix / 2) - 1);
						ok = true;
					}
					else {
						System.out.println("\nDesole votre reponse est incorrect, veillez reprendre svp! ");
					}
				}
				else {
					System.out.println("\nDesole votre reponse est incorrect, veillez reprendre svp! ");
				}
			} 
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}


	/**
	 * Mise a jour du plateau
	 * @param p le plateau du jeu
	 */
	public void updateJoueur(Plateau p) {
		
		// remove all from the list
		this.indicesDesCaseControlees.clear();
		
		for (int i = 0; i<p.taillePlateau(); i++){
			if (p.recupererCase(i).tailleCase() > 0) {
				if (p.recupererCase(i).recupererPiece(p.recupererCase(i).tailleCase() - 1)
						.getCouleur().compareToIgnoreCase(this.couleur) == 0) {
					this.indicesDesCaseControlees.add(p.recupererCase(i).getIndice());
				}
			}
		}
	}


	/**
	 * Choix du nombre de pieces a deplacer
	 * @param p le plateau du jeu
	 * @param choixCase la case choisie par le joueur
	 * @return le nombre de pieces a deplacer
	 */
	public int choisirNombreDePiecesADeplacer(Plateau p, int choixCase) {
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.print("\nVeillez choisir le nombre de pieces a deplacer : ");
				
				// recuperer le choix
				Scanner sc = new Scanner(System.in); 
				choix = sc.nextInt();
				
				// transformer choixCase en i et j
				int i = choixCase / 10;
				int j = choixCase - (i * 10);
				int val = 6 * (i - 1) + (j - 1);
				
				if (0 < choix && choix <= p.recupererCase(val).tailleCase()) {
					// calculer les deplacements possibles
					if (calculerCasesArrivees(choixCase, choix) > 0) {
						ok = true;
					}
					else {
						System.out.println("\nDesole votre choix est impossible a realiser, veillez diminuer le " +
								"nombre de nombre de pieces a deplacer svp! ");
					}
				}
				else if (choix <= 0) {
					System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier positif svp! ");
				}
				else {
					System.out.println("\nDesole " + choix + " est superieur au nombre de pieces disponibles, " +
							"veillez reprendre svp!");
				}
			} 
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}


	/**
	 * Au cas ou le joueur possede des pieces en reserve et qu'il a aussi la
	 * possibilite de deplacer un lot, cette methode permet de choisir l'action a faire
	 * @return une action de jeu
	 * - 0 pour choisir une case que le joueur controle
	 * - 1 pour jouer une piece en reserve
	 */
	public int choisirActionJeu() {
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.println("\nVeillez choisir une action en tapant son numero : ");
				System.out.println("0 - choisir une case que je controle");
				System.out.println("1 - jouer une piece en reserve");
				System.out.print("Reponse : ");
				// recuperer le choix
				Scanner sc = new Scanner(System.in); 
				choix = sc.nextInt();
				if (choix == 0 || choix == 1) {
					ok = true;
				}
				else {
					System.out.println("\nDesole votre reponse est incorrect, veillez reprendre svp! ");
				}
			} 
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}


	/**
	 * Jouer une piece en reserve
	 * @return l'indice de la case choisie par le joueur
	 */
	public int choisirUneCasePourJouerUnePieceEnReserve() {
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.print("\nVeillez choisir une case : ");
				
				// recuperer le choix
				Scanner sc = new Scanner(System.in); 
				choix = sc.nextInt();
				int i = choix / 10;
				int j = choix - (i * 10);
				if (1 <= i && i <= 6 && 1 <= j && j <= 6) {
					ok = true;
				}
				else {
					System.out.println("\nDesole la case choisie n'existe pas, veillez reprendre svp! ");
				}
			} 
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}

}
