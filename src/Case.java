import java.util.*;

/**
 * Classe permettant de creer une case du jeu Focus qui contiendra des pieces
 * @author Charles LADZRO (EI2I4, Polytech, 2020)
 * @version 1.0
 */
public class Case {

	/**
	 * l'indice de la case sur le plateau
	 */
	private final int indice;

	/**
	 * le tableau stockant les pieces de la case
	 */
	private final ArrayList<Piece> listeDePieces;


	/**
	 * Constructeur de la classe Case
	 * @param indice l'indice de la case
	 * @param listeDePieces un tableau contenant des pieces
	 */
	Case(int indice, ArrayList<Piece> listeDePieces) {
		this.indice = indice;
		this.listeDePieces = listeDePieces;
	}

	/**
	 * @return l'indice de la case
	 */
	public int getIndice() {
		return indice;
	}

	/**
	 * Ajoute une piece a la case
	 * @param p la piece a ajouter
	 */
	public void ajouterPiece(Piece p) {
		listeDePieces.add(p);
	}

	/**
	 * Enleve une piece de la case
	 * @param indRemove l'indice de la piece a enlever
	 * @return la piece supprimee
	 */
	public Piece enleverPiece(int indRemove) {
		return listeDePieces.remove(indRemove);
	}

	/**
	 * Recupere une piece de la case
	 * @param id l'indice de la piece a recuperer
	 * @return la piece a recuperer
	 */
	public Piece recupererPiece(int id) {
		return listeDePieces.get(id);
	}

	/**
	 * Donne la taille de la piece
	 * @return la taille de la piece
	 */
	public int tailleCase() {
		return listeDePieces.size();
	}

	/**
	 * Liste toutes les pieces de la case
	 * @return String information de la case
	 */
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (Piece listeDePiece : listeDePieces) {
			str.append(listeDePiece.toString());
		}
		
		return str.toString();
	}
}
