import java.util.*;

/**
 * Classe permettant de creer un plateau qui, dans le jeu, contiendra des cases et se chargera de la mise à jour
 * @author Charles LADZRO (EI2I4, Polytech, 2020)
 * @version 1.0
 */
public class Plateau {


	/**
	 * Array contenant toutes les cases du plateau
	 */
	private final ArrayList<Case> listeDeCases;

	/**
	 * contient l'image du plateau modifiable a tout instant
	 */
	private String imagePlateau;

	/**
	 * Constructeur de la classe Plateau
	 * @param listeDeCases Array contenant toutes les cases du plateau
	 */
	Plateau(ArrayList<Case> listeDeCases) {
		this.listeDeCases = listeDeCases;
	}

	/**
	 * Reperer une case
	 * @param idCase l'id de la case a recuperer
	 * @return la case a recuperer
	 */
	public Case recupererCase(int idCase){
		return listeDeCases.get(idCase);
	}

	/**
	 * Retourne le nombre de case sur le plateau
	 * @return le nombre de case sur le plateau
	 */
	public int taillePlateau(){
		return listeDeCases.size();
	}

	/**
	 * Retourne l'image du plateau
	 * @return String l'image 2d du plateau
	 */
	public String toString() {
		return imagePlateau;
	}

	/**
	 * Methode permettant de mettre a jour le plateau
	 * @param joueurCourantId l'id du joueur actuelle
	 * @param caseChoisieDuJoueur l'indice de la case choisie par le joueur
	 * @param showCaseArrivees indique d'afficher les cases d'arrivee possible qu'a le joueur
	 * @param indicesDesCaseArrivees Array contenant les indices des cases d'arrivee possible
	 * @param joueurColorList Tableau contenant la liste des couleurs
	 * @param joueurReserveList Tableau contenant le nombre de pieces en reserve des joueurs
	 * @param joueurScoreList Tableau contenant le score des joueurs
	 */
	public void updatePlateau(int joueurCourantId, int caseChoisieDuJoueur, 
						     int showCaseArrivees,
							  ArrayList<Integer> indicesDesCaseArrivees, 
							  String[] joueurColorList, int[] joueurReserveList,
							  int[] joueurScoreList) {
		
		imagePlateau = "";

		//motif constante pour l'affichage du plateau
		String motif = "+--------------------+--------------------+--------------------+--------------------" +
				"+--------------------+--------------------+";

		for (int i = 0; i < 6; i++){

			imagePlateau += ("\n" + motif + "\n");
			
			for (int j = 0; j < 6; j++){
				imagePlateau += "|";
				int count = 20;
				
				//Remplissage du plateau avec les pieces
				int val = (6 * i) + j;
				
				imagePlateau += (i + 1) * 10 + (j + 1);
				count -= 2;
				
				for (int k = 0; k < listeDeCases.get(val).tailleCase(); k++) {
					imagePlateau += listeDeCases.get(val).recupererPiece(k).toString();
					count -= 3;
				}
				
				
				//Gestion du visuel joueur
				if (joueurCourantId != -1) {
					if (listeDeCases.get(val).tailleCase() > 0) {
						if (listeDeCases.get(val).recupererPiece(listeDeCases.get(val).tailleCase() - 1)
								.getCouleur().compareToIgnoreCase(joueurColorList[joueurCourantId]) == 0) {
							imagePlateau += Piece.ANSI_YELLOW+"<->"+Piece.ANSI_RESET;
							count -= 3;
						}
					}
				}
				
				if (caseChoisieDuJoueur != -1) {
					if (listeDeCases.get(val).getIndice() == caseChoisieDuJoueur) {
						imagePlateau += Piece.ANSI_YELLOW+"-->"+Piece.ANSI_RESET;
						count -= 3;
					}
				}
				
				if (showCaseArrivees == 1) {
					if (indicesDesCaseArrivees.contains(listeDeCases.get(val).getIndice())) {
						imagePlateau += Piece.ANSI_YELLOW+"<--"+Piece.ANSI_RESET;
						count -= 3;
					}
				}
				
				//Remplissage de l'espace restante par des blancs
				for (int k = 0; k < count; k++) {
					imagePlateau += " ";
				}
				
				
				
			}
			
			imagePlateau += "|";
		}
		
		imagePlateau += ("\n" + motif);
		for (int i = 0; i < joueurColorList.length; i++) {
			imagePlateau += "\n- Reserve " + joueurColorList[i] + " : " + joueurReserveList[i];
			imagePlateau += ", Score : " + joueurScoreList[i];
		}
	}

}
