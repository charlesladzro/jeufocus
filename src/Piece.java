

/**
 * Classe permettant de creer un piece du jeu Focus
 * @author Charles LADZRO (EI2I4, Polytech, 2020)
 * @version 1.0
 */
public class Piece {

	/**
	 * contient la couleur de la piece
	 */
	private final String couleur;
	/**
	 * contient le symbole
	 */
	private final char symbole;

	/**
	 * la couleur par defaut
	 */
	public static final String ANSI_RESET = "\u001B[0m";
	/**
	 * couleur rouge
	 */
	public static final String ANSI_RED = "\u001B[31m";
	/**
	 * couleur verte
	 */
	public static final String ANSI_GREEN = "\u001B[32m";
	/**
	 * couleur jaune
	 */
	public static final String ANSI_YELLOW = "\u001B[33m";
	/**
	 * couleur blanche
	 */
	public static final String ANSI_WHITE = "\u001B[37m";

	/**
	 * Constructeur Piece
	 * @param couleur la couleur de la piece
	 * @param symbole le symbole de la piece
	 */
	Piece(String couleur, char symbole) { 
		this.couleur = couleur;
		this.symbole = symbole;
	}

	/**
	 * Getter de couleur
	 * @return retourne la couleur de la piece
	 */
	public String getCouleur() {
		return couleur;
	}

	/**
	 * Methode d'affichage inherante à l'objet
	 * @return String
	 */
	public String toString() {
		String color;
		
		if (couleur.compareToIgnoreCase("vert") == 0) {
			color = ANSI_GREEN;
		}
		else if (couleur.compareToIgnoreCase("rouge") == 0) {
			color = ANSI_RED;
		}
		else {
			color = ANSI_WHITE;
		}
		
		return color+"["+symbole+"]"+ANSI_RESET;
	}
}
