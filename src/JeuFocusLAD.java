import java.util.*;

/**
 * Classe permettant de creer le jeu FOCUS
 * @author Charles LADZRO (EI2I4, Polytech, 2020)
 * @version 1.0
 */
public class JeuFocusLAD {

	/**
	 * contient le nombre de case sur la ligne du le plateau
	 */
	private final int NOMBRE_DE_CASES_PAR_LIGNE = 6;

	/**
	 * contient le nombre de case sur la colonne du le plateau
	 */
	private final int NOMBRE_DE_CASES_PAR_COLONNE = 6;

	/**
	 * contient le nombre maximum de pieces par case sur le plateau
	 */
	private final int NB_MAX_PIECES_PAR_CASES = 5;

	/**
	 * Le plateau
	 */
	private final Plateau plateau;

	/**
	 * Le nom d'une partie du jeu
	 */
	private final String nomDuJeu;

	/**
	 * Array contenant les joueurs
	 */
	private final ArrayList<Joueur> listeDeJoueur = new ArrayList<>();

	/**
	 * indique l'id du joueur courant
	 */
	private int joueurCourant;

	/**
	 * Tableau contenant les id des joueurs
	 */
	private final int[] joueurIdList = new int[2];

	/**
	 * Tableau contenant la couleur des joueurs
	 */
	private final String[] joueurColorList = new String[2];

	/**
	 * Tableau contenant le nombre de pieces en reserve des joueurs
	 */
	private final int[] joueurReserveList = new int[2];

	/**
	 * Tableau contenant le score des joueurs
	 */
	private final int[] joueurScoreList = new int[2];


	/**
	 * Construecteur de la classe JeuFocusLAD
	 * @param nomDuJeu nom d'une partie du jeu
	 * @param nomDuJoueur1 nom du joueur 1
	 * @param nomDuJoueur2 nom du joueur 2
	 */
	JeuFocusLAD (String nomDuJeu, String nomDuJoueur1, String nomDuJoueur2) {
		this.nomDuJeu = nomDuJeu;
		this.plateau = initJeu();
		this.joueurCourant = 0;
		this.joueurIdList[0] = 0;
		this.joueurIdList[1] = 1;
		this.joueurColorList[0] = "rouge";
		this.joueurColorList[1] = "vert";
		
		// nombre de pieces en reserve pour chaque joueur
		this.joueurReserveList[0] = 0;
		this.joueurReserveList[1] = 0;
		
		// score de chaque joueur
		this.joueurScoreList[0] = 0;
		this.joueurScoreList[1] = 0;
		
		
		// Creation des joueurs
		this.listeDeJoueur.add(new Joueur(nomDuJoueur1, this.joueurColorList[0]));
		this.listeDeJoueur.add(new Joueur(nomDuJoueur2, this.joueurColorList[1]));
	}

	/**
	 * La methode de lancement du jeu
	 */
	public void jouer() {
		System.out.println(this);
		
		int nombreCaseControleeParLeJoueur;
		int nombreReserveDuJoueur;
		int actionJeu = -1;
		int reprendreJeu = 1;
		
		while (reprendreJeu == 1) {
			
			do {
				
				// update joueur courant
				listeDeJoueur.get(joueurCourant).updateJoueur(plateau);
				
				// recuperation du nombre de pieces en reserve pour chaque joueur
				joueurReserveList[0] = listeDeJoueur.get(0).getListeDesPiecesEnReserves().size();
				joueurReserveList[1] = listeDeJoueur.get(1).getListeDesPiecesEnReserves().size();
				
				// recuperation du score de chaque joueur
				joueurScoreList[0] = listeDeJoueur.get(0).getScore();
				joueurScoreList[1] = listeDeJoueur.get(1).getScore();
				
				
				// update du plateau
				plateau.updatePlateau(-1, -1, 0, new ArrayList<>(),
						joueurColorList, joueurReserveList, joueurScoreList);
				
				// recuperation du nombres de cases controlees par le joueur et du nombre de pieces en reserves
				nombreCaseControleeParLeJoueur = listeDeJoueur.get(joueurCourant).getIndicesDesCaseControlees().size();
				nombreReserveDuJoueur = listeDeJoueur.get(joueurCourant).getListeDesPiecesEnReserves().size();
				
				if (nombreCaseControleeParLeJoueur != 0 || nombreReserveDuJoueur != 0) {
					String str = "C'est le tour de ";
					str += listeDeJoueur.get(joueurCourant).getNomDuJoueur();
					System.out.println("\n+------------------+------------------+ " + str + " +------------------+-" +
							"-----------------+");
					
					// Choix des actions à faire en fonction du nombres de cases controlees par le joueur et du nombre
					// de pieces en reserves
					if (nombreCaseControleeParLeJoueur != 0 && nombreReserveDuJoueur != 0) {
						
						// affichage du plateau
						System.out.println(plateau);
						
						actionJeu = listeDeJoueur.get(joueurCourant).choisirActionJeu();
					}
					
					if ( (nombreCaseControleeParLeJoueur != 0 && nombreReserveDuJoueur == 0) || actionJeu == 0) {
						System.out.println("\n+------------------+------------------+ [" +
								listeDeJoueur.get(joueurCourant).getNomDuJoueur() + "] Choix d'une case que "
							+ listeDeJoueur.get(joueurCourant).getNomDuJoueur() +
								" controle +------------------+------------------+");
						plateau.updatePlateau(joueurCourant, -1, 0,
								new ArrayList<>(), joueurColorList, joueurReserveList, joueurScoreList);
						System.out.println(plateau);
						int choixCase = listeDeJoueur.get(joueurCourant).choisirCase();
						
						System.out.println("\n+------------------+------------------+ [" +
								listeDeJoueur.get(joueurCourant).getNomDuJoueur() + "] Choix du nombre de pieces " +
								"a deplacer +------------------+------------------+");
						plateau.updatePlateau(-1, choixCase, 0, new ArrayList<>(),
								joueurColorList, joueurReserveList, joueurScoreList);
						System.out.println(plateau);
						int choisirNombreDePiecesADeplacer =
								listeDeJoueur.get(joueurCourant).choisirNombreDePiecesADeplacer(plateau, choixCase);
						
						System.out.println("\n+------------------+------------------+ [" +
								listeDeJoueur.get(joueurCourant).getNomDuJoueur() + "] Choix de la case d'arrivee " +
								"+------------------+------------------+");
						
						plateau.updatePlateau(-1, choixCase, 1,
								listeDeJoueur.get(joueurCourant).getIndicesDesCaseArrivees(), joueurColorList,
								joueurReserveList, joueurScoreList);
						System.out.println(plateau);
						int choixCaseArrivee = listeDeJoueur.get(joueurCourant).choixCaseArrivee();
						
						// deplacement des pieces
						deplacerLotDePieces(choixCase, choixCaseArrivee, choisirNombreDePiecesADeplacer);
						enleverLotDePiecesSiNecessaire(choixCaseArrivee);
					}
					else if (nombreCaseControleeParLeJoueur == 0 || actionJeu == 1) {
						
						int choixCaseArrivee = listeDeJoueur.get(joueurCourant).choisirUneCasePourJouerUnePieceEnReserve();
						
						// deplacement d'une piece reserve dans une case
						deplacerLotDePieces(choixCaseArrivee);
						enleverLotDePiecesSiNecessaire(choixCaseArrivee);
					}
					
					joueurCourant = (joueurCourant + 1) % 2;
				}
				
				else {
					System.out.println("\n+------------------+------------------+ FIN DU JEU FOCUS " +
							"+------------------+------------------+");
					
					// affichage du plateau
					System.out.println(plateau);
				}
				
				//break;
				
			} while (nombreCaseControleeParLeJoueur != 0 || nombreReserveDuJoueur != 0);
			
			// Fin du jeu
			System.out.println("\nFelicitations " + listeDeJoueur.get((joueurCourant + 1) % 2).getNomDuJoueur()
				+ ", vous venez de gagner contre " + listeDeJoueur.get(joueurCourant).getNomDuJoueur()
				+ " avec un score de " + listeDeJoueur.get((joueurCourant + 1) % 2).getScore() + " - "
				+ listeDeJoueur.get(joueurCourant).getScore() + " !");
				
			reprendreJeu = jouerRevanche();
		}
		
	}

	/**
	 * Methode permettant aux joueurs de choisir de refaire une autre partie ou non
	 * @return le choix des joueurs
	 */
	public int jouerRevanche() {
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			try {
				System.out.println("\nVeillez choisir une action en tapant son numero : ");
				System.out.println("0 - arreter de jouer");
				System.out.println("1 - rejouer une partie");
				System.out.print("Reponse : ");
				// recuperer le choix
				Scanner sc = new Scanner(System.in);
				choix = sc.nextInt();
				if (choix == 0 || choix == 1) {
					ok = true;
				}
				else {
					System.out.println("\nDesole votre reponse est incorrect, veillez reprendre svp! ");
				}
			}
			catch (Exception e) {
				System.out.println("\nDesole votre reponse est incorrect, veillez entrer un entier svp! ");
				ok = false;
			}
		}
		return choix;
	}
	

	/**
	 * Affiche JeuFocusLAD
	 * @return les informations relatifs a la partie de jeu en cours
	 */
	public String toString() {
		String str = "";
		str += "\nNom du jeu : " + nomDuJeu;
		str += "\nNom du joueur1 : " + listeDeJoueur.get(0).getNomDuJoueur() + ", couleur : " +
				listeDeJoueur.get(0).getCouleur();
		str += "\nNom du joueur2 : " + listeDeJoueur.get(1).getNomDuJoueur() + ", couleur : " +
				listeDeJoueur.get(1).getCouleur() + "\n";
		return str;
	}


	/**
	 * Initialise le jeu en creant la plateau avec les pieces en position par defaut
	 */
	public Plateau initJeu() {
		char[] symboleTab = {'+', '+', '*', '*', '+', '+', 
							 '*', '*', '+', '+', '*', '*',
							 '+', '+', '*', '*', '+', '+',
							 '*', '*', '+', '+', '*', '*',
							 '+', '+', '*', '*', '+', '+',
							 '*', '*', '+', '+', '*', '*',
							};
		
		String[] couleurTab = {"rouge", "rouge", "vert", "vert", "rouge", "rouge",
			                   "vert", "vert", "rouge", "rouge", "vert", "vert",
							   "rouge", "rouge", "vert", "vert", "rouge", "rouge",
							   "vert", "vert", "rouge", "rouge", "vert", "vert",
							   "rouge", "rouge", "vert", "vert", "rouge", "rouge",
							   "vert", "vert", "rouge", "rouge", "vert", "vert",
							  };
		
		
		ArrayList<Case> listeDeCases = new ArrayList<>();

		for (int i = 0; i < NOMBRE_DE_CASES_PAR_LIGNE; i++) {
			for (int j = 0; j < NOMBRE_DE_CASES_PAR_COLONNE; j++) {
				int val = 6 * i + j;
				Piece p = new Piece(couleurTab[val], symboleTab[val]);
				ArrayList<Piece> listeDePieces = new ArrayList<>();
				listeDePieces.add(p);
				Case c = new Case((i + 1) * 10 + (j + 1), listeDePieces);
				listeDeCases.add(c);
			}
		}
		
		int[] joueurIdList = {0, 1};
		String[] joueurColorList = {"rouge", "vert"};
		int[] joueurReserveList = {0, 0};

		return new Plateau(listeDeCases);
	}


	/**
	 * Deplacer un lot de pieces d'une case a un autre
	 * @param        choixCase l'indice de la case choisie par le joueur
	 * @param        choixCaseArrivee l'indice de la case d'arrivee choisie par le joueur
	 * @param        choisirNombreDePiecesADeplacer le nombre de pieces a deplacer, choisie par le joueur
	 */
	public void deplacerLotDePieces(int choixCase, int choixCaseArrivee, int choisirNombreDePiecesADeplacer) {
		
		int valD = 6 * ((choixCase / 10) - 1) + ((choixCase - ((choixCase / 10) * 10)) - 1);
		int valA = 6 * ((choixCaseArrivee / 10) - 1) + ((choixCaseArrivee - ((choixCaseArrivee / 10) * 10)) - 1);
		
		int tailleD = plateau.recupererCase(valD).tailleCase();
		int indRemove = tailleD - choisirNombreDePiecesADeplacer;
		
		for (int i = 0; i < choisirNombreDePiecesADeplacer; i++){
			Piece p = plateau.recupererCase(valD).enleverPiece(indRemove);
			plateau.recupererCase(valA).ajouterPiece(p);
		}
		
	}

	/**
	 * Deplacer un lot de pieces de la reserve dans une case
	 * @param choixCaseArrivee l'indice de la case d'arrivee choisie par le joueur
	 */
	public void deplacerLotDePieces(int choixCaseArrivee) {
		
		int valA = 6 * ((choixCaseArrivee / 10) - 1) + ((choixCaseArrivee - ((choixCaseArrivee / 10) * 10)) - 1);
		
		Piece p = listeDeJoueur.get(joueurCourant).getListeDesPiecesEnReserves().remove(0);
		plateau.recupererCase(valA).ajouterPiece(p);
		
	}

	/**
	 * Enlever un lot de piece si le nombre de piece depasse NB_MAX_PIECES_PAR_CASES
	 * @param choixCaseArrivee l'indice de la case d'arrivee choisie par le joueur
	 */
	public void enleverLotDePiecesSiNecessaire(int choixCaseArrivee) {
		
		int valA = 6 * ((choixCaseArrivee / 10) - 1) + ((choixCaseArrivee - ((choixCaseArrivee / 10) * 10)) - 1);
		
		// Gestion du cas ou les pieces sont superieurs a 5
		int tailleA = plateau.recupererCase(valA).tailleCase();
		
		for (int i = 0; i < tailleA - NB_MAX_PIECES_PAR_CASES; i++) {
			Piece p = plateau.recupererCase(valA).enleverPiece(0);
			if (p.getCouleur().compareToIgnoreCase(listeDeJoueur.get(joueurCourant).getCouleur()) == 0) {
				listeDeJoueur.get(joueurCourant).getListeDesPiecesEnReserves().add(p);
			}
			else {
				listeDeJoueur.get(joueurCourant).setScore(listeDeJoueur.get(joueurCourant).getScore() + 1);
			}
		}
	}


	/**
	 * Methode de demarrage du programme de jeu
	 * @param args les arguments en entree du programme
	 */
	public static void main(String[] args) {
		System.out.println("Bienvenue dans le jeu Focus fait par Charles LADZRO ...");
		Scanner clavier = new Scanner(System.in);
		System.out.print("Veillez entrez le nom de Joueur 1: ");
		String nomJ1 = clavier.nextLine();
		System.out.print("Veillez entrez le nom de Joueur 2: ");
		String nomJ2 = clavier.nextLine();

		JeuFocusLAD jfl = new JeuFocusLAD("JeuFocusLAD", nomJ1, nomJ2);
		jfl.jouer();
	}

}
